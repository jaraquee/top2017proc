=begin
This code genreate the SLAC proceeding html page based on the informatino supplied in the "info.yml" file.
=end
require "yaml"
require 'fileutils'


class SlacPage
  def initialize
    @info = YAML.load(File.read("info.yml"))
    @template = File.open("template.html").read
    @lines = []
    fillinfo
  end

  # Fill informatin before filling all talks and posters
  def fillinfo()
    @template.gsub!("@@CONFERENCE@@",@info["conference"])
    @template.gsub!("@@PLACE@@",@info["place"])
    @template.gsub!("@@DATE@@",@info["date"])
    @template.gsub!("@@LOGO@@",@info["logo"])
    @template.gsub!("@@SLACCODE@@",@info["slac_code"])
    @template.gsub!("@@EDITOR@@",@info["editors"].join(", "))
    @template.gsub!("@@COMFILE@@",@info["committee_file"])
  end

  # Get a given field with replacement if it is not presenter
  def get_field(field,repl)
    if field.nil?
      return "<font color=\"red\">#{repl}</font>"
    else
      if field.is_a? Array
        return field.join(", ")
      else
        return field
      end
    end
  end

  # Invited talks
  def get_lines(field)
    lines = []
    if field.nil?
      lines << "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"proceedings_table\">"
      lines << "<tr>"
      lines << "<td align=\"left\" valign=\"top\">This section is still empty</td></tr>"
      lines << "</table>"
      return lines
    end
    field.each do |topic,inf|
      lines << "<div class=\"topic\">#{topic} </div>"
      chairs = get_field(inf["chairs"],"Chairs unknown")
      lines << "<div class=\"chairs_credit\">Session Chairs: #{chairs}</div>"
      lines << "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"proceedings_table\">"
      inf.each do |k,v|
        next if k == "chairs"
        v = {} if v.nil?
        presenter = get_field(v["presenter"],"Presenter unknown")
        lines << "<tr align=\"left\" valign=\"top\">"
        lines << "<td nowrap class=\"author_credit\">#{presenter}</td>"
        lines << "<td class=\"title\">#{k}</td>"
        if !v["arxiv_ref"].nil?
          lines << "<td align=\"center\" nowrap>[ <a href=\"#{arxivlink(v["arxiv_ref"])}\">#{v["arxiv_ref"]}</a> ]</td>"
          lines << "<td align=\"right\" nowrap>[ <a href=\"#{v["pdf"]}\">PDF</a> ]</td>"
        else
          lines << "<td align=\"right\" nowrap> <font color=\"red\"> Missing proceedings</font></td>"
        end
        lines << "</tr>"
      end
      lines << "</table>"
      lines << "<p></p>"
    end
    return lines
  end

  def arxivlink(arx)
    ref = arx.split(":")[1]
    "https://arxiv.org/abs/#{ref}"
  end

  # Create document
  def create_index()
    FileUtils.mkdir_p @info["output_path"]
    file = File.new("#{@info["output_path"]}/index.html","w")
    @template.each_line do |line|
      if line.include?("Invited Talks") && line.include?("h3")
        file.write line
        get_lines(@info["invited_talks"]).each do |l|
          file.write "#{l}\n"
        end
      elsif line.include?("Young Scientists Forum") && line.include?("h3")
        file.write line
        get_lines(@info["young_forum"]).each do |l|
          file.write "#{l}\n"
        end
      elsif line.include?("Posters") && line.include?("h3")
        file.write line
        get_lines(@info["posters"]).each do |l|
          file.write "#{l}\n"
        end
      else
        file.write line
      end
    end
  end
end

sl = SlacPage.new
sl.create_index
